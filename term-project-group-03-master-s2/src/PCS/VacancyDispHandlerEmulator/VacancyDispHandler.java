package PCS.VacancyDispHandlerEmulator;

import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.AppThread;
import AppKickstarter.misc.MBox;
import AppKickstarter.misc.Msg;

public class VacancyDispHandler extends AppThread {
	
	public VacancyDispHandler(String id, AppKickstarter appKickstarter) {
		super(id, appKickstarter);
	}
	
	public void run() {
		MBox atmss = appKickstarter.getThread("PCS").getMBox();
		log.info(id + ": starting...");

		for (boolean quit = false; !quit;) {
			Msg msg = mbox.receive();

			log.fine(id + ": message received: [" + msg + "].");

			switch (msg.getType()) {
			case AddVacancy:
				atmss.send(new Msg(id, mbox, Msg.Type.AddVacancy, msg.getDetails()));
				handleCardInsert();
				break;

			case ReduceVacancy:
				handleCardEject();
				break;

			case Poll:
				atmss.send(new Msg(id, mbox, Msg.Type.PollAck, id + " is up!"));
				break;

			case Terminate:
				quit = true;
				break;

			default:
				log.warning(id + ": unknown message type: [" + msg + "]");
			}
		}

		// declaring our departure
		appKickstarter.unregThread(this);
		log.info(id + ": terminating...");
	} // run

	/**
	 * This method is used to show the information when card is inserted
	 */

	// ------------------------------------------------------------
	// handleCardInsert
	protected void handleCardInsert() {
		log.info(id + ": card inserted");
	} // handleCardInsert

	/**
	 * This method is used to show the information when card is ejected
	 */

	// ------------------------------------------------------------
	// handleCardEject
	protected void handleCardEject() {
		log.info(id + ": card ejected");
	} // handleCardEject
}

