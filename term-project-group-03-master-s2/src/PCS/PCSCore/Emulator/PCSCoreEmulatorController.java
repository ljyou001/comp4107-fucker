package PCS.PCSCore.Emulator;

import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.MBox;
import AppKickstarter.misc.Msg;
import PCS.PCSCore.PCSCore;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

import java.util.logging.Logger;


//======================================================================
// PCSCoreEmulatorControlller
public class PCSCoreEmulatorController {
    private String id;
    private AppKickstarter appKickstarter;
    private Logger log;
    private PCSCore PcsCoreEmulator;
    private MBox pcsMBox;
    public TextArea pcsTextArea;
    private int lineNo = 0;


    //------------------------------------------------------------
    // initialize
    public void initialize(String id, AppKickstarter appKickstarter, Logger log, PCSCore PcsCoreEmulator) {
        this.id = id;
        this.appKickstarter = appKickstarter;
        this.log = log;
        this.PcsCoreEmulator = PcsCoreEmulator;
        this.pcsMBox = appKickstarter.getThread("PCSCore").getMBox();
    } // initialize


    //------------------------------------------------------------
    // buttonPressed
    public void buttonPressed(ActionEvent actionEvent) {
        Button btn = (Button) actionEvent.getSource();

        switch (btn.getText()) {
            case "Gate Open Request":
                pcsMBox.send(new Msg(id, null, Msg.Type.GateOpenRequest, "GateOpenReq"));
                break;

            default:
                log.warning(id + ": unknown button: [" + btn.getText() + "]");
                break;
        }
    } // buttonPressed


    //------------------------------------------------------------
    // appendTextArea
    public void appendTextArea(String status) {
        Platform.runLater(() -> pcsTextArea.appendText(String.format("[%04d] %s\n", ++lineNo, status)));
    } // appendTextArea
} // GateEmulatorController
