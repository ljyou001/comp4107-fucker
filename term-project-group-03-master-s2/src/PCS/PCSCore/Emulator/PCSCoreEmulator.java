package PCS.PCSCore.Emulator;

import AppKickstarter.misc.Msg;
import PCS.PCSCore.PCSCore;
import PCS.PCSStarter;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;


//======================================================================
// PCSCoreEmulator
public class PCSCoreEmulator extends PCSCore {
    private Stage myStage;
    private PCSCoreEmulatorController pcsCoreEmulatorController;
    private final PCSStarter pcsStarter;
    private final String id;


    //------------------------------------------------------------
    // PCSCoreEmulator
    public PCSCoreEmulator(String id, PCSStarter pcsStarter) {
        super(id, pcsStarter);
        this.pcsStarter = pcsStarter;
        this.id = id + "Emulator";
    } // PCSCoreEmulator


    //------------------------------------------------------------
    // start
    public void start() throws Exception {
        Parent root;
        myStage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        String fxmlName = "PCSCore.fxml";
        loader.setLocation(PCSCoreEmulator.class.getResource(fxmlName));
        root = loader.load();
        pcsCoreEmulatorController = (PCSCoreEmulatorController) loader.getController();
        pcsCoreEmulatorController.initialize(id, pcsStarter, log, this);
        myStage.initStyle(StageStyle.DECORATED);
        myStage.setScene(new Scene(root, 840, 470));
        myStage.setTitle("PCS Core");
        myStage.setResizable(false);
        myStage.setOnCloseRequest((WindowEvent event) -> {
            pcsStarter.stopApp();
            Platform.exit();
        });
        myStage.show();
    } // PCSCoreEmulator


    //------------------------------------------------------------
    // processMsg
    protected final boolean processMsg(Msg msg) {
        boolean quit = false;

        switch (msg.getType()) {

            default:
                //quit = super.processMsg(msg);
        }
        return quit;
    } // processMsg


    //------------------------------------------------------------
    // displayMessage
    @Override
    protected void appendTextArea(String Message,String Type) {
        if(Type.equals("Fine")){
            logFine(Message);
        }else if(Type.equals("Info")){
            logInfo(Message);
        }else if(Type.equals("Warning")){
            logWarning(Message);
        }else if(Type.equals("Severe")){
            logSevere(Message);
        }

    } // displayMessage

    //------------------------------------------------------------
    // logFine
    private final void logFine(String logMsg) {
        pcsCoreEmulatorController.appendTextArea("[FINE]: " + logMsg);
        log.fine(id + ": " + logMsg);
    } // logFine


    //------------------------------------------------------------
    // logInfo
    private final void logInfo(String logMsg) {
        pcsCoreEmulatorController.appendTextArea("[INFO]: " + logMsg);
        log.info(id + ": " + logMsg);
    } // logInfo


    //------------------------------------------------------------
    // logWarning
    private final void logWarning(String logMsg) {
        pcsCoreEmulatorController.appendTextArea("[WARNING]: " + logMsg);
        log.warning(id + ": " + logMsg);
    } // logWarning


    //------------------------------------------------------------
    // logSevere
    private final void logSevere(String logMsg) {
        pcsCoreEmulatorController.appendTextArea("[SEVERE]: " + logMsg);
        log.severe(id + ": " + logMsg);
    } // logSevere
} // GateEmulator
