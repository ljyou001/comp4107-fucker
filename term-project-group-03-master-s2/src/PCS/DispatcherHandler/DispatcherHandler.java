package PCS.DispatcherHandler;

import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.*;
import PCS.Classes.Ticket;


//======================================================================
// DispatcherHandler
public class DispatcherHandler extends AppThread {
    protected final MBox pcsCore;
    private DispatcherStatus dispatcherStatus;

    //------------------------------------------------------------
    // DispatcherHandler
    public DispatcherHandler(String id, AppKickstarter appKickstarter) {
        super(id, appKickstarter);
        pcsCore = appKickstarter.getThread("PCSCore").getMBox();
        dispatcherStatus = DispatcherStatus.Idle;
    } // DispatcherHandler


    //------------------------------------------------------------
    // run
    public void run() {
        Thread.currentThread().setName(id);
        log.info(id + ": starting...");

        for (boolean quit = false; !quit;) {
            Msg msg = mbox.receive();

            log.fine(id + ": message received: [" + msg + "].");
            quit = processMsg(msg);
        }

        //declaring our departure
        appKickstarter.unregThread(this);
        log.info(id + ": terminating...");
    } // run


    //------------------------------------------------------------
    // processMsg
    protected boolean processMsg(Msg msg){
        boolean quit = false;

        switch(msg.getType()) {
            case TicketPrintRequest: handleTicketPrintRequest();
                break;
            case TicketDispatchRequest: handleTicketDispatchRequest();
                break;
            case TicketPrintReply: handleTicketPrintReply();
                break;
            case TicketDispatchReply: handleTicketDispatchReply();
                break;
            case Poll:
                handlePollReq();
                break;
            case PollAck:
                handlePollAck();
                break;
            case Terminate:
                quit = true;
                break;
            default:
                log.warning(id + ": unknown message type: [" + msg + "]");
        }
        return quit;
    } // processMsg


    //------------------------------------------------------------
    // handleTicketPrintRequest
    protected final void handleTicketPrintRequest() {
        log.info(id + ": ticket print request received");

        DispatcherStatus oldDispatcherStatus = dispatcherStatus;
        switch (dispatcherStatus) {
            case Idle:
                log.info(id + ": Sending car's entering time to pcs for printing ticket.");
                sendTicketPrintSignal();
                dispatcherStatus = DispatcherStatus.PrintingTicket;
                break;

            case PrintingTicket:
                log.warning(id + ": Ticket is printing, can't print another one!! Ignore request.");
                break;

            case TicketPrinted:
                log.warning(id + ": Ticket is printed!! Ignore request.");
                break;

            case TicketDispatched:
                log.warning(id + ": Ticket is dispatched!! Ignore request.");
                break;
        }

        if (oldDispatcherStatus != dispatcherStatus) {
            log.fine(id + ": dispatcher status change: " + oldDispatcherStatus + " --> " + dispatcherStatus);
        }
    }   // handleTicketPrintRequest


    //------------------------------------------------------------
    // handleTicketDispatchRequest
    protected final void handleTicketDispatchRequest() {
        log.info(id + ": ticket dispatch request received");

        DispatcherStatus oldDispatcherStatus = dispatcherStatus;
        switch (dispatcherStatus) {
            case Idle:
                log.warning(id + ": Ticket is printed, can't print another one!! Ignore request.");
                break;

            case PrintingTicket:
                log.warning(id + ": Ticket is printing!! Ignore request.");
                break;

            case TicketPrinted:
                log.info(id + ": Ticket is printed, waiting for dispatch.");
                sendTicketDispatchSignal();
                dispatcherStatus = DispatcherStatus.TicketDispatched;
                break;

            case TicketDispatched:
                log.warning(id + ": Ticket is dispatched!! Ignore request.");
                break;
        }

        if (oldDispatcherStatus != dispatcherStatus) {
            log.fine(id + ": dispatcher status change: " + oldDispatcherStatus + " --> " + dispatcherStatus);
        }
    }   // handleTicketPrintRequest


    //------------------------------------------------------------
    // handleTicketPrintReply
    protected final void handleTicketPrintReply() {
        log.info(id + ": ticket print reply received");

        DispatcherStatus oldDispatcherStatus = dispatcherStatus;
        switch (dispatcherStatus) {
            case Idle:
                log.warning(id + ": Car's entering time sent to PCS!! Ignore reply.");
                break;

            case PrintingTicket:
                log.info(id + ": inform PCS Core that ticket has finished printing.");
                pcsCore.send(new Msg(id, mbox, Msg.Type.TicketPrintReply, ""));
                dispatcherStatus = DispatcherStatus.TicketPrinted;
                break;

            case TicketPrinted:
                log.warning(id + ": Line 153 - Ticket should be printed!! Ignore reply.");
                break;

            case TicketDispatched:
                log.warning(id + ": Ticket should dispatched!! Ignore reply.");
                break;
        }

        if (oldDispatcherStatus != dispatcherStatus) {
            log.fine(id + ": dispatcher status change: " + oldDispatcherStatus + " --> " + dispatcherStatus);
        }
    }   // handleTicketPrintReply


    //------------------------------------------------------------
    // handleTicketDispatchReply
    protected final void handleTicketDispatchReply() {
        log.info(id + ": ticket dispatch reply received");

        DispatcherStatus oldDispatcherStatus = dispatcherStatus;
        switch (dispatcherStatus) {
            case Idle:
                log.warning(id + ": Car's entering time sent to PCS!! Ignore reply.");
                break;

            case PrintingTicket:
                log.warning(id + ": Ticket should be printing!! Ignore reply.");
                break;

            case TicketPrinted:
                log.warning(id + ": Ticket should be printed!! Ignore reply.");
                break;

            case TicketDispatched:
                log.info(id + ": inform PCS Core that ticket has dispatched.");
                pcsCore.send(new Msg(id, mbox, Msg.Type.TicketDispatchReply, ""));
                dispatcherStatus = DispatcherStatus.Idle;
                break;
        }

        if (oldDispatcherStatus != dispatcherStatus) {
            log.fine(id + ": dispatcher status change: " + oldDispatcherStatus + " --> " + dispatcherStatus);
        }
    }   // handleTicketDispatchReply



    //------------------------------------------------------------
    // handlePollReq
    protected final void handlePollReq() {
        log.info(id + ": poll request received.  Send poll request to hardware.");
        sendPollReq();
    } // handlePollReq


    //------------------------------------------------------------
    // handlePollAck
    protected final void handlePollAck() {
        log.info(id + ": poll ack received.  Send poll ack to PCS Core.");
        pcsCore.send(new Msg(id, mbox, Msg.Type.PollAck, id + " is up!"));
    } // handlePollAck


    //------------------------------------------------------------
    // sendTicketPrintSignal
    protected void sendTicketPrintSignal() {
        // fixme: send ticket print signal to hardware
        log.info(id + ": sending ticket print signal to hardware.");
    } // sendTicketPrintSignal


    //------------------------------------------------------------
    // sendTicketDispatchSignal
    protected void sendTicketDispatchSignal() {
        // fixme: send ticket dispatch signal to hardware
        log.info(id + ": sending ticket dispatch signal to hardware.");
    } // sendTicketDispatchSignal


    //------------------------------------------------------------
    // sendPollReq
    protected void sendPollReq() {
        // fixme: send dispatcher poll request to hardware
        log.info(id + ": poll request received");
    } // sendPollReq


    //------------------------------------------------------------
    // Dispatcher Status
    private enum DispatcherStatus {
        Idle,
        PrintingTicket,
        TicketPrinted,
        TicketDispatched,
    }
} // DispatcherHandler

