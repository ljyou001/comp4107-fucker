package PCS.DispatcherHandler.Emulator;

import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.MBox;
import AppKickstarter.misc.Msg;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;


//======================================================================
// DispatcherEmulatorController
public class DispatcherEmulatorController {
    private String id;
    private AppKickstarter appKickstarter;
    private Logger log;
    private DispatcherEmulator dispatcherEmulator;
    private MBox dispatcherMBox;
    public TextArea dispatcherTextArea;
    public Button autoPollButton;
    private int lineNo = 0;


    //------------------------------------------------------------
    // initialize
    public void initialize(String id, AppKickstarter appKickstarter, Logger log, DispatcherEmulator dispatcherEmulator) {
        this.id = id;
        this.appKickstarter = appKickstarter;
        this.log = log;
        this.dispatcherEmulator = dispatcherEmulator;
        this.dispatcherMBox = appKickstarter.getThread("DispatcherHandler").getMBox();
    } // initialize


    //------------------------------------------------------------
    // buttonPressed
    public void buttonPressed(ActionEvent actionEvent) {
        Button btn = (Button) actionEvent.getSource();

        switch (btn.getText()) {
            case "Ticket Print Request":
                dispatcherMBox.send(new Msg(id, null, Msg.Type.TicketPrintRequest, "TicketPrintReq"));
                break;

            case "Ticket Dispatch Request":
                dispatcherMBox.send(new Msg(id, null, Msg.Type.TicketDispatchRequest, "TicketDispatchReq"));
                break;

            case "Ticket Print Reply":
                dispatcherMBox.send(new Msg(id, null, Msg.Type.TicketPrintReply, "TicketPrintReply"));
                break;

            case "Ticket Dispatch Reply":
                dispatcherMBox.send(new Msg(id, null, Msg.Type.TicketDispatchReply, "TicketDispatchReply"));
                break;

            case "Poll Request":
                appendTextArea("Send poll request.");
                dispatcherMBox.send(new Msg(id, null, Msg.Type.Poll, ""));
                break;

            case "Poll ACK":
                appendTextArea("Send poll ack.");
                dispatcherMBox.send(new Msg(id, null, Msg.Type.PollAck, ""));
                break;

            case "Auto Poll: On":
                Platform.runLater(() -> autoPollButton.setText("Auto Poll: Off"));
                dispatcherMBox.send(new Msg(id, null, Msg.Type.DispatcherEmulatorAutoPollToggle, "ToggleAutoPoll"));
                break;

            case "Auto Poll: Off":
                Platform.runLater(() -> autoPollButton.setText("Auto Poll: On"));
                dispatcherMBox.send(new Msg(id, null, Msg.Type.DispatcherEmulatorAutoPollToggle, "ToggleAutoPoll"));
                break;

            default:
                log.warning(id + ": unknown button: [" + btn.getText() + "]");
                break;
        }
    } // buttonPressed


    //------------------------------------------------------------
    // appendTextArea
    public void appendTextArea(String status) {
        Platform.runLater(() -> dispatcherTextArea.appendText(String.format("[%04d] %s\n", ++lineNo, status)));
    } // appendTextArea

} // DispatcherEmulatorController

