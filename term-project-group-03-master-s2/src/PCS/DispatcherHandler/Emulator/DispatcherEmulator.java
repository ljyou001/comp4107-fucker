package PCS.DispatcherHandler.Emulator;

import AppKickstarter.misc.*;

import PCS.PCSStarter;
import PCS.DispatcherHandler.DispatcherHandler;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

//======================================================================
// DispatcherEmulator
public class DispatcherEmulator extends DispatcherHandler {
    private Stage myStage;
    private DispatcherEmulatorController dispatcherEmulatorController;
    private final PCSStarter pcsStarter;
    private final String id;
    private boolean autoPoll;


    //------------------------------------------------------------
    // DispatcherEmulator
    public DispatcherEmulator(String id, PCSStarter pcsStarter) {
        super(id, pcsStarter);
        this.pcsStarter = pcsStarter;
        this.id = id + "Emulator";
        this.autoPoll = true;
    } // DispatcherEmulator


    //------------------------------------------------------------
    // start
    public void start() throws Exception {
        Parent root;
        myStage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        String fxmlName = "DispatcherEmulator.fxml";
        loader.setLocation(DispatcherEmulator.class.getResource(fxmlName));
        root = loader.load();
        dispatcherEmulatorController = (DispatcherEmulatorController) loader.getController();
        dispatcherEmulatorController.initialize(id, pcsStarter, log, this);
        myStage.initStyle(StageStyle.DECORATED);
        myStage.setScene(new Scene(root, 420, 470));
        myStage.setTitle("Dispatcher Emulator");
        myStage.setResizable(false);
        myStage.setOnCloseRequest((WindowEvent event) -> {
            pcsStarter.stopApp();
            Platform.exit();
        });
        myStage.show();
    } // DispatcherEmulator


    //------------------------------------------------------------
    // processMsg
    protected final boolean processMsg(Msg msg) {
        boolean quit = false;

        switch (msg.getType()) {
            case DispatcherEmulatorAutoPollToggle:
                handleDispatcherEmulatorAutoPollToggle();
                break;
            default:
                quit = super.processMsg(msg);
        }
        return quit;
    } // processMsg


    //------------------------------------------------------------
    // sendTicketPrintSignal
    @Override
    protected void sendTicketPrintSignal() {
        logFine("Ticket print signal received.");
    } // sendTicketPrintSignal


    //------------------------------------------------------------
    // sendTicketDispatchSignal
    @Override
    protected void sendTicketDispatchSignal() {
        logFine("Ticket dispatch signal received. Send open gate signal to pcs.");
    } // sendTicketDispatchSignal


    //------------------------------------------------------------
    // sendPollReq
    @Override
    protected void sendPollReq() {
        logFine("Poll request received.  [autoPoll is " + (autoPoll ? "on]" : "off]"));
        if (autoPoll) {
            logFine("Send poll ack.");
            mbox.send(new Msg(id, mbox, Msg.Type.PollAck, ""));
        }
    } // sendPollReq


    //------------------------------------------------------------
    // handleDispatcherEmulatorAutoPollToggle:
    public final boolean handleDispatcherEmulatorAutoPollToggle() {
        autoPoll = !autoPoll;
        logFine("Auto poll change: " + (autoPoll ? "off --> on" : "on --> off"));
        return autoPoll;
    } // handleDispatcherEmulatorAutoPollToggle


    //------------------------------------------------------------
    // logFine
    private final void logFine(String logMsg) {
        dispatcherEmulatorController.appendTextArea("[FINE]: " + logMsg);
        log.fine(id + ": " + logMsg);
    } // logFine


    //------------------------------------------------------------
    // logInfo
    private final void logInfo(String logMsg) {
        dispatcherEmulatorController.appendTextArea("[INFO]: " + logMsg);
        log.info(id + ": " + logMsg);
    } // logInfo


    //------------------------------------------------------------
    // logWarning
    private final void logWarning(String logMsg) {
        dispatcherEmulatorController.appendTextArea("[WARNING]: " + logMsg);
        log.warning(id + ": " + logMsg);
    } // logWarning


    //------------------------------------------------------------
    // logSevere
    private final void logSevere(String logMsg) {
        dispatcherEmulatorController.appendTextArea("[SEVERE]: " + logMsg);
        log.severe(id + ": " + logMsg);
    } // logSevere

} // DispatcherEmulator

